vim.cmd([[:so $HOME/.config/nvim/nolua.vim]])

require("core")
require("config")
require("keymaps")
